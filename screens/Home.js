import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  useWindowDimensions,
} from "react-native";
import AppbarStyles from "./styles/AppbarStyles";
import { TabView, SceneMap, TabBar } from "react-native-tab-view";
import { Ionicons, Entypo, MaterialCommunityIcons } from "@expo/vector-icons";
import LocationPage from "./Location";
import { Card } from "react-native-paper";

const men = [
  {
    cardTitle: "MEN ESSENTIAL MASSAGE",
    cardImage: require("../assets/images/splash1.jpg"),
  },
  {
    cardTitle: "MEN HAIR COLOR",
    cardImage: require("../assets/images/splash2.jpg"),
  },
  {
    cardTitle: "MEN CUT AND BLOW DRY",
    cardImage: require("../assets/images/splash3.jpg"),
  },
  {
    cardTitle: "MEX WAXING",
    cardImage: require("../assets/images/splash5.jpg"),
  },
  {
    cardTitle: "FACIAL",
    cardImage: require("../assets/images/homepage2.jpg"),
  },
];
const women = [
  {
    cardTitle: "WOMEN ESSENTIAL MASSAGE",
    cardImage: require("../assets/images/splash1.jpg"),
  },
  {
    cardTitle: "WOMEN HAIR COLOR",
    cardImage: require("../assets/images/splash2.jpg"),
  },
  {
    cardTitle: "CUT AND BLOW DRY",
    cardImage: require("../assets/images/splash3.jpg"),
  },
  {
    cardTitle: "WAXING",
    cardImage: require("../assets/images/splash5.jpg"),
  },
  {
    cardTitle: "FACIAL",
    cardImage: require("../assets/images/homepage1.jpg"),
  },
  {
    cardTitle: "LASHES",
    cardImage: require("../assets/images/homepage2.jpg"),
  },
  {
    cardTitle: "NAILS",
    cardImage: require("../assets/images/homepage3.jpg"),
  },
  {
    cardTitle: "BROWS",
    cardImage: require("../assets/images/homepage1.jpg"),
  },
];
const packages = [
  {
    cardTitle: "ESSENTIAL MASSAGE PACKAGE",
    cardImage: require("../assets/images/splash1.jpg"),
  },
  {
    cardTitle: "HAIR COLOR PACKAGE",
    cardImage: require("../assets/images/splash2.jpg"),
  },
  {
    cardTitle: "CUT AND BLOW DRY PACKAGE",
    cardImage: require("../assets/images/splash3.jpg"),
  },
  {
    cardTitle: "WAXING PACKAGE",
    cardImage: require("../assets/images/splash5.jpg"),
  },
  {
    cardTitle: "FACIALS PACKAGE",
    cardImage: require("../assets/images/homepage1.jpg"),
  },
  {
    cardTitle: "LASHES PACKAGE",
    cardImage: require("../assets/images/homepage2.jpg"),
  },
  {
    cardTitle: "NAILS PACKAGE",
    cardImage: require("../assets/images/homepage3.jpg"),
  },
  {
    cardTitle: "BROWS PACKAGE",
    cardImage: require("../assets/images/homepage1.jpg"),
  },
];

export default function Home({ navigation }) {
  const [ActiveTab, setActiveTab] = useState("men");
  const [showLocationPage, setShowLocationPage] = useState(false);
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    { key: "first", title: "Female" },
    { key: "second", title: "Men" },
    { key: "third", title: "Packages" },
  ]);

  const FirstRoute = (navigation) => (
    <ScrollView
      contentContainerStyle={{ flexGrow: 1 }}
      showsVerticalScrollIndicator={false}
    >
      {women.map((item, index) => (
        <TouchableOpacity
          onPress={() =>
            navigation.navigate("Service", {
              Service: item.cardTitle,
            })
          }
          key={index}
          style={styles.listItems}
        >
          <View>
            <Card style={{ marginHorizontal: 15 }}>
              <Card.Cover source={item.cardImage} style={{ height: 150 }} />
              <Card.Content style={{ padding: 15 }}>
                <Text style={styles.cardText}>{item.cardTitle}</Text>
              </Card.Content>
            </Card>
          </View>
        </TouchableOpacity>
      ))}
    </ScrollView>
  );

  const SecondRoute = (navigation) => (
    <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
      {men.map((item, index) => (
        <TouchableOpacity
          onPress={() =>
            navigation.navigate("Service", {
              Service: item.cardTitle,
            })
          }
          key={index}
          style={styles.listItems}
        >
          <View>
            <Card style={{ marginHorizontal: 15 }}>
              <Card.Cover source={item.cardImage} style={{ height: 150 }} />
              <Card.Content style={{ padding: 15 }}>
                <Text style={styles.cardText}>{item.cardTitle}</Text>
              </Card.Content>
            </Card>
          </View>
        </TouchableOpacity>
      ))}
    </ScrollView>
  );

  const ThirdRoute = (navigation) => (
    <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
      {packages.map((item, index) => (
        <TouchableOpacity
          onPress={() =>
            navigation.navigate("Service", {
              Service: item.cardTitle,
            })
          }
          key={index}
          style={styles.listItems}
        >
          <View>
            <Card style={{ marginHorizontal: 15 }}>
              <Card.Cover source={item.cardImage} style={{ height: 150 }} />
              <Card.Content style={{ padding: 15 }}>
                <Text style={styles.cardText}>{item.cardTitle}</Text>
              </Card.Content>
            </Card>
          </View>
        </TouchableOpacity>
      ))}
    </ScrollView>
  );

  const renderScene = SceneMap({
    first: FirstRoute,
    second: SecondRoute,
    third: ThirdRoute,
  });

  const renderTabBar = (props) => (
    <TabBar
      {...props}
      indicatorStyle={{ backgroundColor: "#e2c798" }}
      style={{ backgroundColor: "black" }}
    />
  );

  const layout = useWindowDimensions();

  const LocationPageBackHandler = () => {
    setShowLocationPage(false);
  };

  return (
    <View style={styles.container}>
      <LocationPage
        visible={showLocationPage}
        onBack={LocationPageBackHandler}
      />
      <View style={AppbarStyles.TitleContainer}>
        <TouchableOpacity
          style={AppbarStyles.TouchContainer}
          onPress={() => navigation.openDrawer()}
        >
          <Ionicons name="ios-menu" size={30} color="black" />
        </TouchableOpacity>
        <Text style={AppbarStyles.TitleText}>Home</Text>
        <View style={AppbarStyles.rightContainer}>
          <View style={styles.HeaderIconsContainer}>
            <TouchableOpacity onPress={() => setShowLocationPage(true)}>
              <Entypo
                name="location-pin"
                size={24}
                color="black"
                style={styles.locationIcon}
              />
            </TouchableOpacity>
            <TouchableOpacity>
              <MaterialCommunityIcons
                name="shopping-outline"
                size={24}
                color="black"
              />
            </TouchableOpacity>
          </View>
        </View>
      </View>
      <TabView
        renderTabBar={renderTabBar}
        navigationState={{ index, routes }}
        renderScene={SceneMap({
          first: () => FirstRoute(navigation),
          second: () => SecondRoute(navigation),
          third: () => ThirdRoute(navigation),
        })}
        onIndexChange={setIndex}
        initialLayout={{ width: layout.width }}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    marginTop: "7%",
    flex: 1,
    backgroundColor: "#f0eff5",
  },
  ScreenTitleContainer: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: 40,
    height: 70,
  },
  ScreenTitleText: {
    textAlign: "center",
    fontSize: 18,
  },
  ScreenTitleTextContainer: {
    width: "75%",
  },
  username: {
    textAlign: "center",
    fontSize: 20,
    fontWeight: "bold",
    marginVertical: 10,
  },
  TabsContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginVertical: 20,
    marginHorizontal: 10,
  },
  active: {
    backgroundColor: "#a379b6",
  },
  TabText: {
    color: "white",
  },
  listItems: {
    marginVertical: 10,
  },
  MostPopularContainer: {
    marginVertical: 20,
  },
  MostPopularText: {
    fontSize: 16,
    fontWeight: "bold",
    marginHorizontal: 10,
  },
  popularItems: {
    marginVertical: 10,
    marginHorizontal: 10,
  },
  HeaderIconsContainer: {
    flexDirection: "row",
  },
  locationIcon: {
    marginRight: 10,
  },
});
