import React, { useState } from "react";
import { View, Text, StyleSheet, TouchableOpacity, Image } from "react-native";
import { MaterialCommunityIcons, Ionicons } from "@expo/vector-icons";
import { ScrollView } from "react-native-gesture-handler";
import AppbarStyles from "./styles/AppbarStyles";
import FormStyles from "./styles/FormContainer";
import { Divider } from "react-native-paper";

export default function ServiceDetails({ route, navigation }) {
  const [selectedAddonsState, setSelectedAddOnsState] = useState([]);
  const AddonsList = [
    {
      Id: 1,
      Addon: "Men essential Massage",
      Duration: "15",
      Price: 67,
    },
    {
      Id: 2,
      Addon: "Men Hair Color",
      Duration: "15",
      Price: 65,
    },
    {
      Id: 3,
      Addon: "Men Cut and Blow Dry",
      Duration: "15",
      Price: 89,
    },
    {
      Id: 4,
      Addon: "Mens Waxing",
      Duration: "15",
      Price: 57,
    },
    {
      Id: 5,
      Addon: "Mens Facials",
      Duration: "15",
      Price: 50,
    },
  ];
  const SelectedAddonsLists = [];

  const AddonClickHandler = (id) => {
    console.log(id);
    let selectedArray = selectedAddonsState;
    const index = selectedArray.indexOf(id);
    if (index > -1) {
      selectedArray.splice(index, 1);
    } else {
      selectedArray.push(id);
    }
    setSelectedAddOnsState(selectedArray);
  };
  const { ServiceName } = route.params;
  return (
    <View style={styles.container}>
      <View style={AppbarStyles.TitleContainer}>
        <TouchableOpacity
          onPress={() => navigation.goBack()}
          style={AppbarStyles.TouchContainer}
        >
          <Image source={require("../assets/images/Back.png")} />
          <Text style={AppbarStyles.BackText}>Back</Text>
        </TouchableOpacity>
        <Text style={AppbarStyles.TitleText}>Service Details</Text>
        <View style={AppbarStyles.rightContainer} />
      </View>
      <ScrollView
        style={styles.ScrollViewStyle}
        showsVerticalScrollIndicator={false}
      >
        <View style={styles.DetailsContainer}>
          <View>
            <Text style={styles.DetailHeading}>SERVICE NAME</Text>
            <Text style={styles.DetailHeadingData}>{ServiceName}</Text>
          </View>
          <View>
            <View style={styles.durationIconText}>
              <MaterialCommunityIcons name="clock" size={20} />
              <Text style={styles.DetailHeading}>Duration</Text>
            </View>
            <Text style={styles.DetailHeadingData}>15:00</Text>
          </View>
          <View>
            <Text style={styles.DetailHeading}>€Price</Text>
            <Text style={styles.DetailHeadingData}>€65</Text>
          </View>
        </View>
        <View style={styles.ExtraTitleContainer}>
          <Text style={styles.ExtraTitleText}>SERVICE EXTRA</Text>
        </View>
        <View style={styles.AddonContainer}>
          {AddonsList.map((item, index) => (
            <View key={item.Id}>
              <View style={styles.AddonItems}>
                <View>
                  <Text>{item.Addon}</Text>
                  <Text>
                    Duration: {item.Duration} | Price: €{item.Price}
                  </Text>
                </View>
                <View>
                  <TouchableOpacity onPress={() => AddonClickHandler(item.Id)}>
                    <MaterialCommunityIcons
                      name={
                        selectedAddonsState.some((saved) => saved == item.Id)
                          ? "check-circle"
                          : "plus-circle-outline"
                      }
                      size={24}
                      color="#e2c798"
                    />
                  </TouchableOpacity>
                </View>
              </View>
              <Divider />
            </View>
          ))}
        </View>
        <View style={styles.ExtraTitleContainer}>
          <Text style={styles.ExtraTitleText}>MORE INFO</Text>
        </View>
        <View style={styles.ExpectContainer}>
          <Text style={styles.MoreinfoTitles}>What to expect</Text>
          <Text>
            Details of the service and the procedure along with an insight into
            the overall experience.
          </Text>
        </View>
        <View>
          <Text style={styles.MoreinfoTitles}>How to Prepare</Text>
          <Text>
            Instructions to the client on the pre requisitres and steps needed
            to be done before the stylist arrives for the service.
          </Text>
        </View>
      </ScrollView>
      <View style={styles.bottom}>
        <TouchableOpacity
          onPress={() =>
            navigation.navigate("DateTime", {
              ServiceName: ServiceName,
            })
          }
        >
          <View style={FormStyles.SignInButton}>
            <Text style={{ fontSize: 20 }}>Continue</Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    marginTop: "7%",
    flex: 1,
  },
  ScreenTitleContainer: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: 20,
    height: 70,
  },
  ScreenTitleText: {
    width: "90%",
    textAlign: "center",
    fontSize: 18,
  },
  DetailsContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginVertical: 10,
  },
  DetailHeading: {
    fontWeight: "bold",
    textAlign: "center",
  },
  durationIconText: {
    flexDirection: "row",
    alignItems: "center",
  },
  DetailHeadingData: {
    marginVertical: 15,
    textAlign: "center",
  },
  ExtraTitleText: {
    color: "#F2994A",
  },
  ExtraTitleContainer: {
    borderBottomColor: "#F2994A",
    borderBottomWidth: 1,
    paddingBottom: 10,
  },
  AddonItems: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    height: 60,
  },
  AddonContainer: {
    marginVertical: 15,
  },
  ExpectContainer: {
    marginVertical: 15,
  },
  MoreinfoTitles: {
    fontWeight: "bold",
  },
  bottom: {
    flex: 1,
    justifyContent: "flex-end",
    marginBottom: 20,
  },
  ContinueButton: {
    backgroundColor: "#F2994A",
    height: 40,
    justifyContent: "center",
    alignItems: "center",
    marginHorizontal: 30,
    borderRadius: 25,
  },
  ScrollViewStyle: {
    marginHorizontal: 20,
  },
});
