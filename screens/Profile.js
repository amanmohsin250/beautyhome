import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  Image,
  TouchableWithoutFeedback,
  Keyboard,
} from "react-native";
import { Entypo, Ionicons, MaterialIcons } from "@expo/vector-icons";
import AppbarStyles from "./styles/AppbarStyles";
import FormStyles from "./styles/FormContainer";

export default function Profile({ navigation }) {
  const [activeTabs, setActiveTabs] = useState("Profile");
  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss()}>
      <View style={styles.container}>
        <View style={AppbarStyles.TitleContainer}>
          <TouchableOpacity
            onPress={() => navigation.goBack()}
            style={AppbarStyles.TouchContainer}
          >
            <Image source={require("../assets/images/Back.png")} />
            <Text style={AppbarStyles.BackText}>Back</Text>
          </TouchableOpacity>
          <Text style={AppbarStyles.TitleText}>Profile</Text>
          <View style={AppbarStyles.rightContainer} />
        </View>
        <View style={styles.ProfileTabsContainer}>
          {activeTabs == "Profile" ? (
            <TouchableOpacity
              activeOpacity={0.8}
              style={styles.ActiveTab}
              onPress={() => setActiveTabs("Profile")}
            >
              <View>
                <Ionicons
                  name="person"
                  size={24}
                  color="#e2c798"
                  style={styles.TabIcon}
                />
                <Text style={styles.ActiveTabText}>Profile</Text>
              </View>
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              activeOpacity={0.8}
              style={styles.InactiveTab}
              onPress={() => setActiveTabs("Profile")}
            >
              <View>
                <Ionicons
                  name="person"
                  size={24}
                  color="black"
                  style={styles.TabIcon}
                />
                <Text style={styles.TabText}>Profile</Text>
              </View>
            </TouchableOpacity>
          )}

          {activeTabs == "Reset" ? (
            <TouchableOpacity
              activeOpacity={0.8}
              style={styles.ActiveTab}
              onPress={() => setActiveTabs("Reset")}
            >
              <View>
                <Ionicons
                  name="lock-closed"
                  size={24}
                  color="#e2c798"
                  style={styles.TabIcon}
                />
                <Text style={styles.ActiveTabText}>Reset Password</Text>
              </View>
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              activeOpacity={0.8}
              style={styles.InactiveTab}
              onPress={() => setActiveTabs("Reset")}
            >
              <View>
                <Ionicons
                  name="lock-closed"
                  size={24}
                  color="black"
                  style={styles.TabIcon}
                />
                <Text style={styles.TabText}>Reset Password</Text>
              </View>
            </TouchableOpacity>
          )}
        </View>

        {activeTabs == "Profile" ? (
          <View style={styles.fieldsContainer}>
            <View style={styles.fieldsRow}>
              <MaterialIcons name="email" size={34} color="#e2c798" />
              <View style={styles.input}>
                <TextInput placeholder="Email" value="aman@devnation.com" />
              </View>
            </View>
            <View style={styles.fieldsRow}>
              <Entypo name="mobile" size={34} color="#e2c798" />
              <View style={styles.input}>
                <TextInput value="03176238087" keyboardType="number-pad" />
              </View>
            </View>
          </View>
        ) : (
          <View style={styles.fieldsContainer}>
            <View style={styles.fieldsRow}>
              <MaterialIcons name="email" size={34} color="#e2c798" />
              <View style={styles.input}>
                <TextInput placeholder="Enter your email address here" />
              </View>
            </View>
          </View>
        )}

        {activeTabs == "Profile" ? (
          <View style={styles.bottom}>
            <TouchableOpacity>
              <View style={FormStyles.SignInButton}>
                <Text style={{ fontSize: 20 }}>Update</Text>
              </View>
            </TouchableOpacity>
          </View>
        ) : (
          <View style={styles.bottom}>
            <TouchableOpacity>
              <View style={FormStyles.SignInButton}>
                <Text style={{ fontSize: 20 }}>Reset</Text>
              </View>
            </TouchableOpacity>
          </View>
        )}
      </View>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  container: {
    marginTop: "7%",
    flex: 1,
    backgroundColor: "white",
  },
  ScreenTitleContainer: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: 20,
    height: 70,
  },
  ScreenTitleText: {
    width: "100%",
    textAlign: "center",
    fontSize: 18,
  },
  ProfileTabsContainer: {
    marginTop: 15,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  ActiveTabText: {
    textAlign: "center",
    color: "#e2c798",
  },
  TabText: {
    textAlign: "center",
  },
  ActiveTab: {
    width: "50%",
    paddingBottom: 10,
    borderBottomWidth: 2,
    borderBottomColor: "#e2c798",
  },
  InactiveTab: {
    width: "50%",
    paddingBottom: 10,
  },
  TabIcon: {
    alignSelf: "center",
  },
  fieldsContainer: {
    marginTop: 30,
  },
  input: {
    width: "85%",
    // borderWidth: 1,
    // borderColor: "grey",
    padding: 10,
  },
  fieldsRow: {
    marginHorizontal: 10,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    height: 60,
  },
  bottom: {
    flex: 1,
    justifyContent: "flex-end",
    marginBottom: 40,
  },
  UpdateButton: {
    backgroundColor: "#C4C4C4",
    height: 40,
    justifyContent: "center",
    alignItems: "center",
    marginHorizontal: 30,
    borderRadius: 25,
  },
});
