import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  Modal,
  TouchableOpacity,
  Image,
} from "react-native";
import { AntDesign } from "@expo/vector-icons";
import AppbarStyles from "./styles/AppbarStyles";

export default function Location(props) {
  const [selectedCountry, setSelectedCountry] = useState("Helsinki");
  const Countries = ["Helsinki", "Espoo", "Vantaa"];
  return (
    <Modal visible={props.visible} animationType="slide">
      <View style={styles.LocationContainer}>
        <View style={AppbarStyles.TitleContainer}>
          <TouchableOpacity
            onPress={props.onBack}
            style={AppbarStyles.TouchContainer}
          >
            <Image source={require("../assets/images/Back.png")} />
            <Text style={AppbarStyles.BackText}>Back</Text>
          </TouchableOpacity>
          <Text style={AppbarStyles.TitleText}>Select Location</Text>
          <View style={AppbarStyles.rightContainer} />
        </View>

        {Countries.map((item, index) => (
          <TouchableOpacity
            onPress={() => setSelectedCountry(item)}
            key={index}
          >
            <View style={styles.ListItems}>
              <View style={styles.IconContainer}>
                {item == selectedCountry ? (
                  <AntDesign name="check" size={24} color="black" />
                ) : null}
              </View>
              <View style={styles.CountryContainer}>
                <Text>{item}</Text>
              </View>
            </View>
          </TouchableOpacity>
        ))}
      </View>
    </Modal>
  );
}

const styles = StyleSheet.create({
  LocationContainer: {
    marginTop: "7%",
    flex: 1,
  },
  TitleContainer: {
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "#C4C4C4",
    height: 50,
  },
  TitleText: {
    width: "90%",
    textAlign: "center",
  },
  ListItems: {
    margin: 20,
    flexDirection: "row",
    alignItems: "center",
  },
  IconContainer: {
    width: "10%",
  },
  CountryContainer: {
    width: "85%",
  },
});
