import React from "react";
import { View, Text, StyleSheet, Image, TouchableOpacity } from "react-native";
import { Entypo, Ionicons } from "@expo/vector-icons";
import AppbarStyles from "./styles/AppbarStyles";

export default function Confirmation({ route, navigation }) {
  const { ServiceName, selectedDate, timing } = route.params;
  return (
    <View style={styles.container}>
      <View style={AppbarStyles.TitleContainer}>
        <TouchableOpacity
          onPress={() => navigation.goBack()}
          style={AppbarStyles.TouchContainer}
        >
          <Image source={require("../assets/images/Back.png")} />
          <Text style={AppbarStyles.BackText}>Back</Text>
        </TouchableOpacity>
        <Text style={AppbarStyles.TitleText}>Your pro is on the way</Text>
        <View style={AppbarStyles.rightContainer} />
      </View>
      <Image
        source={require("../assets/images/Congratulations.png")}
        style={styles.Image}
      />
      <View style={styles.InfodisplayContainer}>
        <View style={styles.Infodisplay}>
          <Text style={styles.ArriveText}>
            Your Pro will arrive on {selectedDate.substring(0, 16)} at {timing}
          </Text>
        </View>
      </View>
      <View style={styles.ServicesContainer}>
        <Text>{ServiceName}</Text>
      </View>
      <View style={styles.GetReadyContainer}>
        <Entypo name="open-book" size={20} color="black" />
        <Text style={styles.GetreadyText}>How to get ready</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    marginTop: "7%",
    flex: 1,
    backgroundColor: "white",
  },
  ScreenTitleContainer: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: 20,
    height: 70,
  },
  ScreenTitleText: {
    width: "90%",
    textAlign: "center",
    fontSize: 18,
  },
  heading: {
    textAlign: "center",
    fontSize: 16,
    fontWeight: "bold",
  },
  Image: {
    resizeMode: "contain",
    width: "100%",
    height: 200,
  },
  Infodisplay: {
    width: 200,
    height: 200,
    borderColor: "grey",
    borderWidth: 3,
    padding: 30,
    borderRadius: 200 / 2,
    justifyContent: "center",
  },
  ArriveText: {
    textAlign: "center",
    fontSize: 15,
    fontWeight: "bold",
  },
  InfodisplayContainer: {
    alignItems: "center",
    top: -30,
  },
  ServicesContainer: {
    marginVertical: 25,
    borderColor: "grey",
    borderRadius: 10,
    borderWidth: 1,
    backgroundColor: "#C4C4C4",
    padding: 10,
    marginHorizontal: 20,
  },
  GetReadyContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginHorizontal: 20,
  },
  GetreadyText: {
    width: "90%",
  },
});
