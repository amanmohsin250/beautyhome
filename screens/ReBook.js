import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
} from "react-native";
import OurServices from "./OurServices";
import AppbarStyles from "./styles/AppbarStyles";
import FormStyles from "./styles/FormContainer";

export default function Rebook() {
  const [showServices, setShowServices] = useState(false);
  const ServicesBackHandler = () => {
    setShowServices(false);
  };
  var past = [
    {
      service: "Wax and Blow Dry",
      Date: "05 june 2021",
      Time: "05:00",
    },
    {
      service: "Mens HairCut",
      Date: "03 june 2021",
      Time: "01:00",
    },
    {
      service: "Women essentials",
      Date: "01 june 2021",
      Time: "12:00",
    },
  ];
  return (
    <View style={styles.container}>
      <View style={AppbarStyles.TitleContainer}>
        <View style={AppbarStyles.TouchContainer} />
        <Text style={AppbarStyles.TitleText}>Rebook your Pro</Text>
        <View style={AppbarStyles.rightContainer} />
      </View>
      <OurServices visible={showServices} onBack={ServicesBackHandler} />
      <ScrollView>
        {past.map((upcomingitem, index) => (
          <View key={index} style={styles.listitem}>
            <Text>{upcomingitem.service}</Text>
            <Text>
              {upcomingitem.Date}/{upcomingitem.Time}
            </Text>
          </View>
        ))}
      </ScrollView>
      <View style={styles.bottom}>
        <TouchableOpacity onPress={() => setShowServices(true)}>
          <View style={FormStyles.SignInButton}>
            <Text style={{ fontSize: 20 }}>Browse Our Services</Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    marginTop: "7%",
    flex: 1,
  },
  ScreenTitleContainer: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: 20,
    height: 70,
  },
  ScreenTitleText: {
    width: "100%",
    textAlign: "center",
    fontSize: 18,
  },
  listitem: {
    backgroundColor: "white",
    flexDirection: "row",
    height: 50,
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: 15,
    marginTop: 10,
  },
  bottom: {
    flex: 1,
    justifyContent: "flex-end",
    marginBottom: 20,
  },
  browseButton: {
    backgroundColor: "#C4C4C4",
    height: 40,
    justifyContent: "center",
    alignItems: "center",
    marginHorizontal: 30,
    borderRadius: 25,
  },
});
