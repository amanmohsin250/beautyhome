import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Image,
} from "react-native";
import AppbarStyles from "./styles/AppbarStyles";

export default function Booking() {
  const [tab, setTab] = useState("upcoming");
  var past = [
    {
      service: "Wax and Blow Dry",
      Date: "05 june 2021",
      Time: "05:00",
    },
    {
      service: "Mens HairCut",
      Date: "03 june 2021",
      Time: "01:00",
    },
    {
      service: "Women essentials",
      Date: "01 june 2021",
      Time: "12:00",
    },
  ];
  var upcoming = [
    {
      service: "Wax and Blow Dry",
      Date: "18 june 2021",
      Time: "05:00",
    },
  ];
  return (
    <View style={styles.container}>
      <View style={AppbarStyles.TitleContainer}>
        <View style={AppbarStyles.TouchContainer} />
        <Text style={AppbarStyles.TitleText}>Booking History</Text>
        <View style={AppbarStyles.rightContainer} />
      </View>
      <View style={styles.TopTabs}>
        <TouchableOpacity onPress={() => setTab("upcoming")}>
          <View style={tab == "upcoming" ? styles.active : styles.inactive}>
            <Text>UPCOMING</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => setTab("past")}>
          <View style={tab == "past" ? styles.active : styles.inactive}>
            <Text>PAST</Text>
          </View>
        </TouchableOpacity>
      </View>
      <ScrollView>
        {tab == "upcoming"
          ? upcoming.map((upcomingitem, index) => (
              <View key={index} style={styles.listitem}>
                <Text>{upcomingitem.service}</Text>
                <Text>
                  {upcomingitem.Date}/{upcomingitem.Time}
                </Text>
              </View>
            ))
          : past.map((upcomingitem, index) => (
              <View key={index} style={styles.listitem}>
                <Text>{upcomingitem.service}</Text>
                <Text>
                  {upcomingitem.Date}/{upcomingitem.Time}
                </Text>
              </View>
            ))}
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    marginTop: "7%",
    flex: 1,
  },
  ScreenTitleContainer: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: 20,
    height: 70,
  },
  ScreenTitleText: {
    width: "100%",
    textAlign: "center",
    fontSize: 18,
  },
  TopTabs: {
    flexDirection: "row",
    justifyContent: "space-around",
    marginBottom: 10,
  },
  active: {
    borderBottomColor: "blue",
    borderBottomWidth: 2,
    padding: 10,
  },
  inactive: {
    padding: 10,
  },
  listitem: {
    backgroundColor: "white",
    flexDirection: "row",
    height: 50,
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: 15,
    marginTop: 10,
  },
});
