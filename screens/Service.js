import React from "react";
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Image,
} from "react-native";
import ServiceSlider from "../components/ServiceSlider";
import { MaterialIcons } from "@expo/vector-icons";
import { Chip, Divider } from "react-native-paper";
import AppbarStyles from "./styles/AppbarStyles";

const images = [
  require("../assets/images/splash1.jpg"),
  require("../assets/images/splash2.jpg"),
  require("../assets/images/splash3.jpg"),
  require("../assets/images/splash5.jpg"),
];

export default function Service({ route, navigation }) {
  const { Service } = route.params;
  const ServicesList = [
    Service,
    Service,
    Service,
    Service,
    Service,
    Service,
    Service,
  ];
  return (
    <View style={styles.container}>
      <View style={AppbarStyles.TitleContainer}>
        <TouchableOpacity
          onPress={() => navigation.goBack()}
          style={AppbarStyles.TouchContainer}
        >
          <Image source={require("../assets/images/Back.png")} />
          <Text style={AppbarStyles.BackText}>Back</Text>
        </TouchableOpacity>
        <Text style={AppbarStyles.TitleText}>{Service}</Text>
        <View style={AppbarStyles.rightContainer} />
      </View>
      {/* <Text style={styles.servicename}>{Service}</Text> */}
      <ServiceSlider images={images} />
      <View style={styles.ratingContainer}>
        <Text style={styles.StarsText}>
          rated 4 out of 5 based on 2466 reviews
        </Text>
      </View>
      <ScrollView style={styles.ServicesContainer}>
        {ServicesList.map((item, index) => (
          <TouchableOpacity
            key={index}
            onPress={() =>
              navigation.navigate("ServiceDetails", {
                ServiceName: item,
              })
            }
          >
            <View style={styles.listItems}>
              <Text style={{ width: "70%", fontSize: 12 }}>{item}</Text>
              <Text>€69</Text>
              <MaterialIcons name="navigate-next" size={34} color="#e2c798" />
            </View>
            <Divider />
          </TouchableOpacity>
        ))}
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    marginTop: "7%",
    flex: 1,
  },
  ScreenTitleContainer: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: 20,
    height: 70,
  },
  ScreenTitleText: {
    width: "90%",
    textAlign: "center",
    fontSize: 18,
  },
  servicename: {
    textAlign: "center",
    fontSize: 16,
    fontWeight: "bold",
    marginVertical: 10,
  },
  StarsText: {
    textAlign: "center",
  },
  listItems: {
    marginVertical: 5,
    padding: 10,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  ServicesContainer: {
    // marginVertical: 20,
  },
  ratingContainer: {
    backgroundColor: "#e2c798",
    height: 60,
    justifyContent: "center",
    top: -20,
  },
});
