import React from "react";
import { View, Text, StyleSheet, Image, TouchableOpacity } from "react-native";
import { Entypo } from "@expo/vector-icons";
import AppbarStyles from "./styles/AppbarStyles";
import FormStyles from "./styles/FormContainer";

export default function Refer() {
  return (
    <View style={styles.container}>
      <View style={AppbarStyles.TitleContainer}>
        <View style={AppbarStyles.TouchContainer} />
        <Text style={AppbarStyles.TitleText}>Refer a friend</Text>
        <View style={AppbarStyles.rightContainer} />
      </View>
      <View style={styles.TopTitle}>
        <Text style={styles.title}>€15 off for your friends!</Text>
        <Text style={styles.title}>
          Know a friend who is WFH? Give them €15 off a relaxing massage at home
          and get €15 off yourself
        </Text>
      </View>
      <Image
        source={require("../assets/images/Refferal.png")}
        style={styles.ReferalImage}
      />
      <View style={styles.codeSection}>
        <Text>Share your invite code</Text>
        <View style={styles.inviteBoxSection}>
          <View style={styles.inviteBox}>
            <Text>ABGR74F</Text>
          </View>
          <View>
            <TouchableOpacity>
              <Entypo name="share-alternative" size={24} color="black" />
            </TouchableOpacity>
          </View>
        </View>
      </View>
      <View style={styles.bottom}>
        <TouchableOpacity>
          <View style={FormStyles.SignInButton}>
            <Text style={{ fontSize: 20 }}>Invite Friends</Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    marginTop: "7%",
    flex: 1,
    backgroundColor: "white",
  },
  ScreenTitleContainer: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: 20,
    height: 70,
  },
  ScreenTitleText: {
    width: "100%",
    textAlign: "center",
    fontSize: 18,
  },
  TopTitle: {
    margin: 20,
    paddingHorizontal: 20,
  },
  title: {
    textAlign: "center",
    marginBottom: 15,
  },
  ReferalImage: {
    width: "100%",
    height: "50%",
    resizeMode: "contain",
  },
  codeSection: {
    marginHorizontal: 20,
  },
  inviteBox: {
    borderColor: "black",
    borderWidth: 1,
    padding: 10,
    width: "85%",
    marginRight: 15,
  },
  inviteBoxSection: {
    flexDirection: "row",
    alignItems: "center",
  },
  bottom: {
    flex: 1,
    justifyContent: "flex-end",
    marginBottom: 20,
  },
  InviteButton: {
    backgroundColor: "#C4C4C4",
    height: 40,
    justifyContent: "center",
    alignItems: "center",
    marginHorizontal: 20,
    borderRadius: 25,
  },
});
