import React from "react";
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Image,
} from "react-native";
import { Entypo, Ionicons } from "@expo/vector-icons";
import AppbarStyles from "./styles/AppbarStyles";
import FormStyles from "./styles/FormContainer";

export default function Payment({ route, navigation }) {
  const { ServiceName, selectedDate, timing } = route.params;
  return (
    <View style={styles.container}>
      <View style={AppbarStyles.TitleContainer}>
        <TouchableOpacity
          onPress={() => navigation.goBack()}
          style={AppbarStyles.TouchContainer}
        >
          <Image source={require("../assets/images/Back.png")} />
          <Text style={AppbarStyles.BackText}>Back</Text>
        </TouchableOpacity>
        <Text style={AppbarStyles.TitleText}>Checkout</Text>
        <View style={AppbarStyles.rightContainer} />
      </View>
      <View style={styles.ScreenContainer}>
        <View style={styles.PaymentInputFieldsView}>
          <Entypo name="location-pin" size={20} color="#e2c798" />
          <TextInput
            placeholder="Client's Address"
            style={styles.Input}
            multiline={true}
            numberOfLines={4}
          />
        </View>
        <View style={styles.PaymentInputFieldsView}>
          <Entypo name="shopping-cart" size={20} color="#e2c798" />
          <View style={styles.ProductInfoView}>
            <Text>{ServiceName}</Text>
            <Text>€69 | 15 mins</Text>
          </View>
        </View>
        <View>
          <TextInput
            style={styles.TextArea}
            multiline={true}
            numberOfLines={4}
            placeholder="Enter any instructions for your beauty professional here. E.g. there is a free parking, the doorbell doesn’t work, etc."
          />
        </View>
        <View style={styles.PaymentInputFieldsView}>
          <Entypo name="clock" size={20} color="#e2c798" />
          <Text>
            Appointment on {selectedDate.substring(0, 16)} at {timing}
          </Text>
        </View>
        <TouchableOpacity>
          <View style={styles.PaymentInputFieldsView}>
            <Entypo name="credit-card" size={24} color="#e2c798" />
            <Text style={styles.AddnewPaymentText}>Add new Payment method</Text>
          </View>
        </TouchableOpacity>
        <View style={styles.PaymentInputFieldsView}>
          <Entypo name="price-tag" size={20} color="#e2c798" />
          <TextInput
            placeholder="Enter your Discount Code"
            style={styles.Input}
          />
        </View>
      </View>
      <View style={styles.Bottom}>
        <View style={styles.PaymentInputFieldsView}>
          <Text>Subtotal</Text>
          <Text>€Price</Text>
        </View>
        <View style={styles.PaymentInputFieldsView}>
          <Text>Total</Text>
          <Text>€Price</Text>
        </View>
        <TouchableOpacity
          onPress={() =>
            navigation.navigate("Confirmation", {
              ServiceName: ServiceName,
              selectedDate: selectedDate,
              timing: timing,
            })
          }
        >
          <View style={FormStyles.SignInButton}>
            <Text style={{ fontSize: 20 }}>Pay Securely</Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    marginTop: "7%",
    flex: 1,
    backgroundColor: "white",
  },
  ScreenTitleContainer: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: 20,
    height: 70,
  },
  ScreenTitleText: {
    width: "90%",
    textAlign: "center",
    fontSize: 18,
  },
  Input: {
    borderColor: "grey",
    borderWidth: 1,
    padding: 10,
    borderRadius: 10,
    width: "90%",
    backgroundColor: "white",
  },
  PaymentInputFieldsView: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginVertical: 10,
  },
  ProductInfoView: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginVertical: 10,
    width: "85%",
  },
  TextArea: {
    borderColor: "grey",
    borderWidth: 1,
    padding: 10,
    borderRadius: 10,
    backgroundColor: "white",
    height: 70,
    marginBottom: 10,
  },
  AddnewPaymentText: {
    width: "85%",
    fontWeight: "bold",
    color: "#e2c798",
  },
  Bottom: {
    flex: 1,
    justifyContent: "flex-end",
    marginBottom: 20,
    marginHorizontal: 30,
  },
  PayButton: {
    backgroundColor: "#C4C4C4",
    height: 40,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 25,
    marginTop: 20,
  },
  ScreenContainer: {
    margin: 20,
  },
});
