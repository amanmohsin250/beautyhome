import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Alert,
  Image,
} from "react-native";
import CalendarPicker from "react-native-calendar-picker";
import { Ionicons, MaterialIcons } from "@expo/vector-icons";
import { Chip, Divider } from "react-native-paper";
import AppbarStyles from "./styles/AppbarStyles";

var timing = [
  "02:00",
  "03:00",
  "04:00",
  "05:00",
  "06:00",
  "07:00",
  "08:00",
  "09:00",
  "10:00",
];

export default function DateTime({ route, navigation }) {
  const { ServiceName } = route.params;
  const [selectedDate, setSelectedDate] = useState();
  const onDateChange = (date) => {
    setSelectedDate(date);
  };
  const startDate = selectedDate ? selectedDate.toString() : "";
  return (
    <View style={styles.container}>
      <View style={AppbarStyles.TitleContainer}>
        <TouchableOpacity
          onPress={() => navigation.goBack()}
          style={AppbarStyles.TouchContainer}
        >
          <Image source={require("../assets/images/Back.png")} />
          <Text style={AppbarStyles.BackText}>Back</Text>
        </TouchableOpacity>
        <Text style={AppbarStyles.TitleText}>Select Date and Time</Text>
        <View style={AppbarStyles.rightContainer} />
      </View>
      <View style={styles.CalendarContainer}>
        <CalendarPicker onDateChange={onDateChange} />
      </View>
      <ScrollView
        showsVerticalScrollIndicator={false}
        style={styles.ScrollViewStyle}
      >
        {timing.map((item, index) => (
          <TouchableOpacity
            key={index}
            onPress={() =>
              Alert.alert(
                "Confirm",
                "Do you want to continue ordering other things or Proceed to checkout",
                [
                  {
                    text: "Proceed to checkout",
                    onPress: () =>
                      navigation.navigate("Payment", {
                        ServiceName: ServiceName,
                        selectedDate: startDate,
                        timing: item,
                      }),
                  },
                  {
                    text: "Continue",
                    onPress: () => navigation.navigate("Home"),
                  },
                ]
              )
            }
          >
            <View style={styles.listItems}>
              <Text>{item}</Text>
              <MaterialIcons name="navigate-next" size={34} color="#e2c798" />
            </View>
            <Divider />
          </TouchableOpacity>
        ))}
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    marginTop: "7%",
    flex: 1,
  },
  ScreenTitleContainer: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: 20,

    height: 70,
  },
  ScreenTitleText: {
    width: "90%",
    textAlign: "center",
    fontSize: 18,
  },
  CalendarContainer: {
    margin: 20,
  },
  listItems: {
    // marginVertical: 5,
    // borderColor: "black",
    // borderWidth: 1,
    // borderRadius: 15,
    // padding: 15,
    // backgroundColor: "white",
    // flexDirection: "row",
    // justifyContent: "space-between",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    height: 50,
  },
  ScrollViewStyle: {
    margin: 20,
  },
});
