import React from "react";
import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { createStackNavigator } from "@react-navigation/stack";
import { Entypo, AntDesign } from "@expo/vector-icons";
import HomeScreen from "./Home";
import BookingScreen from "./Booking";
import ReBookScreen from "./ReBook";
import ReferScreen from "./Refer";

const Tab = createMaterialBottomTabNavigator();
const BookingStack = createStackNavigator();
const RebookStack = createStackNavigator();
const ReferStack = createStackNavigator();

const BookingStacks = () => {
  return (
    <BookingStack.Navigator>
      <BookingStack.Screen
        name="Booking"
        component={BookingScreen}
        options={{ title: "Booking History" }}
      />
    </BookingStack.Navigator>
  );
};

const RebookStacks = () => {
  return (
    <RebookStack.Navigator>
      <RebookStack.Screen
        name="Rebook"
        component={ReBookScreen}
        options={{ title: "Rebook your pro" }}
      />
    </RebookStack.Navigator>
  );
};

const ReferStacks = () => {
  return (
    <ReferStack.Navigator>
      <ReferStack.Screen
        name="Refer"
        component={ReferScreen}
        options={{ title: "Refer a friend" }}
      />
    </ReferStack.Navigator>
  );
};

export default function HomeTabs() {
  return (
    <Tab.Navigator
      initialRouteName="Home"
      activeColor="#e2c798"
      barStyle={{ backgroundColor: "#ffffff" }}
    >
      <Tab.Screen
        name="Home"
        component={HomeScreen}
        options={{
          tabBarLabel: "Home",
          tabBarIcon: ({ color }) => (
            <Entypo name="home" size={26} color={color} />
          ),
        }}
      />
      <Tab.Screen
        name="Bookings"
        component={BookingScreen}
        options={{
          tabBarLabel: "Bookings",
          tabBarIcon: ({ color }) => (
            <Entypo name="book" size={26} color={color} />
          ),
        }}
      />
      <Tab.Screen
        name="Rebook"
        component={ReBookScreen}
        options={{
          tabBarLabel: "Rebook",
          tabBarIcon: ({ color }) => (
            <Entypo name="retweet" size={26} color={color} />
          ),
        }}
      />
      <Tab.Screen
        name="Refer"
        component={ReferScreen}
        options={{
          tabBarLabel: "Refer",
          tabBarIcon: ({ color }) => (
            <AntDesign name="gift" size={26} color={color} />
          ),
        }}
      />
    </Tab.Navigator>
  );
}
