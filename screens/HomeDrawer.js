import React from "react";
import { createDrawerNavigator } from "@react-navigation/drawer";
import HomeTabs from "./HomeTabs";
import DrawerContent from "./DrawerContent";

const Drawer = createDrawerNavigator();

export default function HomeDrawer() {
  return (
    <Drawer.Navigator
      initialRouteName="Home"
      drawerContent={(props) => <DrawerContent {...props} />}
    >
      <Drawer.Screen name="Home" component={HomeTabs} />
    </Drawer.Navigator>
  );
}
