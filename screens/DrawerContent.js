import React from "react";
import { View, Text, StyleSheet, Image, TouchableOpacity } from "react-native";
import { Avatar, Title, Caption, Drawer } from "react-native-paper";
import { DrawerContentScrollView, DrawerItem } from "@react-navigation/drawer";
import {
  Ionicons,
  MaterialIcons,
  Feather,
  Entypo,
  MaterialCommunityIcons,
} from "@expo/vector-icons";

export default function DrawerContent(props) {
  const ManageAccountHandler = () => {
    props.navigation.closeDrawer();
    props.navigation.navigate("Profile");
  };
  return (
    <View style={styles.drawerContainer}>
      <Drawer.Section style={styles.ProfileSection}>
        <View style={styles.AvatarSection}>
          <Avatar.Image
            source={require("../assets/images/Avatar.png")}
            size={70}
          />
          <Title style={styles.NameText}>Aman</Title>
        </View>
        <TouchableOpacity onPress={ManageAccountHandler}>
          <View style={styles.drawerRow}>
            <Text>Manage Account</Text>
            <Ionicons name="chevron-forward" size={24} color="black" />
          </View>
        </TouchableOpacity>
      </Drawer.Section>
      <DrawerContentScrollView>
        <View style={styles.drawerRow}>
          <Ionicons name="home-outline" size={24} color="black" />
          <Text style={styles.ItemText}>Home</Text>
        </View>

        <View style={styles.drawerRow}>
          <MaterialIcons name="payment" size={24} color="black" />
          <Text style={styles.ItemText}>Payments</Text>
        </View>

        <View style={styles.drawerRow}>
          <Feather name="shopping-bag" size={24} color="black" />
          <Text style={styles.ItemText}>Shop</Text>
        </View>

        <View style={styles.drawerRow}>
          <Ionicons name="gift-outline" size={24} color="black" />
          <Text style={styles.ItemText}>Gift Cards</Text>
        </View>

        <View style={styles.drawerRow}>
          <Entypo name="book" size={24} color="black" />
          <Text style={styles.ItemText}>How to get Ready</Text>
        </View>

        <View style={styles.drawerRow}>
          <MaterialIcons name="support-agent" size={24} color="black" />
          <Text style={styles.ItemText}>Support</Text>
        </View>

        <View style={styles.drawerRow}>
          <Entypo name="chat" size={24} color="black" />
          <Text style={styles.ItemText}>FAQs</Text>
        </View>

        <View style={styles.drawerRow}>
          <MaterialIcons name="privacy-tip" size={24} color="black" />
          <Text style={styles.ItemText}>Privacy policy</Text>
        </View>

        <View style={styles.drawerRow}>
          <MaterialCommunityIcons
            name="map-marker-distance"
            size={24}
            color="black"
          />
          <Text style={styles.ItemText}>Our Journey</Text>
        </View>
      </DrawerContentScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  drawerContainer: {
    flex: 1,
    marginHorizontal: 20,
  },
  ProfileSection: {
    marginTop: 50,
  },
  AvatarSection: {
    flexDirection: "row",
    alignItems: "center",
    marginVertical: 10,
  },
  NameText: {
    marginHorizontal: 20,
  },
  drawerRow: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginVertical: 10,
  },
  ItemText: {
    width: "80%",
    fontSize: 18,
  },
});
