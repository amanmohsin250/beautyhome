import React, { useState } from "react";
import {
  View,
  Modal,
  Text,
  TextInput,
  StyleSheet,
  Image,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Keyboard,
} from "react-native";
import { MaterialCommunityIcons as Icon } from "@expo/vector-icons";
import AppbarStyles from './styles/AppbarStyles';
import FormStyles from './styles/FormContainer';

export default function SignIn(props) {
  const [hidePass, setHidePass] = useState(true);
  const [EmailText, setEmailText] = useState("");
  const [PasswordText, setPasswordText] = useState("");
  const [signInErr, setSignInErr] = useState(false);

  const HandlePasswordVisibility = () => {
    setHidePass(!hidePass);
  };
  const EmailHandler = (enteredText) => {
    setEmailText(enteredText);
  };
  const PasswordHandler = (enteredText) => {
    setPasswordText(enteredText);
  };
  const SignInHandler = () => {
    if (EmailText == "Aman" && PasswordText == "Hello") {
      props.onBack;
      props.navigation.replace("Home");
      setSignInErr(false);
    } else {
      setSignInErr(true);
    }
  };
  return (
    <Modal visible={props.visible} animationType="slide">
      <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>

        <View style={styles.SignInContainer}>


         {/* Registration Bar */}
         <View style={AppbarStyles.TitleContainer}>
              <TouchableOpacity onPress={props.onBack} style={AppbarStyles.TouchContainer}>
                <Image source={require("../assets/images/Back.png")}/>
                <Text style={AppbarStyles.BackText}>Back</Text>
              </TouchableOpacity>
              <Text style={AppbarStyles.TitleText}>Sign In</Text>
              <View style={AppbarStyles.rightContainer}/>
            </View>

            <View style={styles.FormContainer}>

            {/* Register Component */}
            <View style={styles.RegisterViewContainer}>
              <TouchableOpacity activeOpacity={0.8}>
                <View style={styles.FaceBookButton}>
                  <Icon name="facebook-square" size={15} color="#fff" />
                  <Text style={{ marginLeft: 10, color: "white" }}>Register With Facebook</Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity activeOpacity={0.8}>
                <View style={styles.AppleButton}>
                  <Icon name="apple" size={15} color="#fff" />
                  <Text style={{ marginLeft: 10, color: "white" }}>Register With Apple</Text>
                </View>
              </TouchableOpacity>
            </View>
            {/* Register Component Ends */}

          <View style={styles.FormContainer}>
            <TouchableOpacity activeOpacity={0.8}>
              <View style={styles.FaceBookButton}>
                <Text style={{ color: "white" }}>Login With Facebook</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={0.8}>
              <View style={styles.AppleButton}>
                <Text style={{ color: "white" }}>Signin With Apple</Text>
              </View>
            </TouchableOpacity>


            <Text style={styles.OrText}>OR</Text>
            {signInErr ? (
              <Text style={styles.error}>Invalid Email or Password</Text>
            ) : null}

            <View style={styles.input}>
              <TextInput
                placeholder="Email"
                onChangeText={EmailHandler}
                value={EmailText}
              />
            </View>
            <View style={styles.password}>
              <TextInput
                placeholder="password"
                secureTextEntry={hidePass}
                style={{ flex: 1 }}
                onChangeText={PasswordHandler}
                value={PasswordText}
              />
              <TouchableOpacity onPress={HandlePasswordVisibility}>
                <Icon name={hidePass ? "eye" : "eye-off"} size={20} />
              </TouchableOpacity>
            </View>
            <TouchableOpacity>
              <Text style={styles.forgotText}>Forgot Password?</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.bottom}>
            <TouchableOpacity>
              <Text style={styles.termsText}>Terms and Conditions</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={SignInHandler}>
              <View style={styles.SignInButton}>
                <Text style={{ fontSize: 20 }}>Sign In</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </TouchableWithoutFeedback>
    </Modal>
  );
}

const styles = StyleSheet.create({
  SignInContainer: {
    marginTop: 50,
    flex: 1,
  },
  TitleContainer: {
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "#C4C4C4",
    height: 50,
  },
  TitleText: {
    width: "90%",
    textAlign: "center",
  },
  FormContainer: {
    marginTop: 30,
  },
  FaceBookButton: {
    height: 40,
    backgroundColor: "#2A89BF",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 20,
  },
  AppleButton: {
    height: 40,
    backgroundColor: "#000000",
    justifyContent: "center",
    alignItems: "center",
  },
  OrText: {
    fontSize: 20,
    fontWeight: "bold",
    textAlign: "center",
    marginVertical: 40,
  },
  input: {
    marginHorizontal: 30,
    borderWidth: 1,
    borderColor: "grey",
    padding: 10,
    marginBottom: 20,
  },
  password: {
    marginHorizontal: 30,
    borderWidth: 1,
    borderColor: "grey",
    padding: 10,
    flexDirection: "row",
    marginBottom: 10,
  },
  forgotText: {
    textAlign: "center",
  },
  bottom: {
    flex: 1,
    justifyContent: "flex-end",
    marginBottom: 40,
  },
  SignInButton: {
    backgroundColor: "#C4C4C4",
    height: 40,
    justifyContent: "center",
    alignItems: "center",
    marginHorizontal: 30,
    borderRadius: 25,
  },
  termsText: {
    marginBottom: 30,
    textAlign: "center",
  },
  error: {
    color: "red",
    textAlign: "center",
    marginBottom: 10,
  },
});
