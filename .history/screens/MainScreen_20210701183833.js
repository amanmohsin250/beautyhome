import React, { useState } from "react";
import { StyleSheet, Text, View, TouchableOpacity, Image, ImageBackground } from "react-native";
import Slider from "../components/Slider";
import SignIn from "./SignIn";
import SignUp from "./SignUp";
import OurServices from "./OurServices";

const images = [
  require("../assets/images/splash1.jpg"),
  require("../assets/images/splash2.jpg"),
  require("../assets/images/splash3.jpg"),
  require("../assets/images/splash5.jpg"),
];

export default function MainScreen({ navigation }) {
  const [showSignIn, setShowSignIn] = useState(false);
  const [showSignUp, setShowSignUp] = useState(false);
  const [showServices, setShowServices] = useState(false);

  const SignInBackHandler = () => {
    setShowSignIn(false);
  };
  const SignUpBackHandler = () => {
    setShowSignUp(false);
  };
  const ServicesBackHandler = () => {
    setShowServices(false);
  };

  const image = { url: "" }

  return (
    <View style={styles.container}>
      <ImageBackground source={require("../assets/images/main-makeup.jpg")} style={styles.image}>
     
      <SignIn
        visible={showSignIn}
        onBack={SignInBackHandler}
        navigation={navigation}
      />
      <SignUp visible={showSignUp} onBack={SignUpBackHandler} />
      <OurServices visible={showServices} onBack={ServicesBackHandler} />
      <Image
        source={require("../assets/images/white-logo-fleek.jpg")}
        style={styles.Logo}
      />
      {/* <View style={styles.Title}>
        <Text style={{ fontSize: 20, fontWeight: "bold" }}>Beauty@Home</Text>
      </View>
      <Slider images={images} /> */}
      <View style={styles.bottom}>
        <View style={styles.ButtonsSection}>
          <TouchableOpacity
            style={styles.SignInButton}
            onPress={() => setShowSignIn(true)}
          >
            <View>
              <Text style={{}}>Sign in</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.SignupButton}
            onPress={() => setShowSignUp(true)}
          >
            <View>
              <Text style={{color: "black", fontSize: 15}}>Sign up</Text>
            </View>
          </TouchableOpacity>
        </View>
        <View>
          <TouchableOpacity
            style={styles.BrowseSection}
            onPress={() => setShowServices(true)}
          >
            <View>
              <Text style={{ color: "#fff", fontWeight: "800" }}>Browse our Services</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
      </ImageBackground>
    </View>
  );
}

const styles = StyleSheet.create({
  container: { 
    marginTop: 20, 
    flex: 1, 
  },
  image: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center"
  },
  ScreenTitleContainer: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: 20,
    height: 70,
  },
  ScreenTitleText: {
    width: "100%",
    textAlign: "center",
    fontSize: 18,
  },
  Title: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  ButtonsSection: {
    margin: 50,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  SignInButton: {
    backgroundColor: "#fff",
    height: 50,
    width: "45%",
    borderRadius: 5,
    justifyContent: "center",
    alignItems: "center",
  },
  SignupButton: {
    backgroundColor: "#e2c798",
    height: 50,
    width: "45%",
    borderRadius: 5,
    justifyContent: "center",
    alignItems: "center",
  },
  BrowseSection: {
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 10,
  },
  bottom: {
    flex: 1,
    justifyContent: "flex-end",
    marginBottom: 20,
  },
  Logo: {
    height: "20%",
    width: "100%",
    resizeMode: "contain",
    marginTop: "10%",
    justifyContent: "center",
    alignItems: "center",
  },
});
