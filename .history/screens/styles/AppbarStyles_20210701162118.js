import {StyleSheet} from 'react-native';


export default StyleSheet.create({
    SignUpContainer: {
      marginTop: "10%",
      flex: 1,
    },
    TitleContainer: {
      flexDirection: "row",
      alignItems: "center",
      backgroundColor: "#fff",
      height: 50,
      borderColor: "black",
      borderWidth: 2,
      ...Platform.select({
        ios: {
          shadowColor: '#000',
          shadowOffset: {width: 1, height: 3},
          shadowOpacity: 0.2,
        },
        android: {
          elevation: 4,
        },
      })
    },
    TitleText: {
      width: "70%",
      borderColor: "black",
      borderWidth: 2,
      textAlign: "center",
      fontSize: 16,
      fontWeight: "500"
    },
    BackText: {
        fontSize: 12,
      fontWeight: "600"
    },
    TouchContainer: {
        borderColor: 'black',
        borderWidth: 2,
        width: "20%",
        display: "flex",
        flexDirection: "row",
        alignItems: "center"
    }
    
    
  });
