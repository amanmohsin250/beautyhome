import {StyleSheet} from 'react-native';


export default StyleSheet.create({
    SignUpContainer: {
      marginTop: "10%",
      flex: 1,
    },
    TitleContainer: {
      flexDirection: "row",
      alignItems: "center",
      backgroundColor: "#fff",
      height: 50,
      borderColor: "black",
      borderWidth: 2,
      ...Platform.select({
        ios: {
          shadowColor: '#000',
          shadowOffset: {width: 1, height: 3},
          shadowOpacity: 0.2,
        },
        android: {
          elevation: 4,
        },
      })
    },
    TitleText: {
      width: "70%",
      borderColor: "black",
      borderWidth: 2,
      textAlign: "center",
      fontSize: 16,
      fontWeight: "500"
    },
    FormContainer: {
      marginTop: 30,
    },
    FaceBookButton: {
      height: 40,
      backgroundColor: "#2A89BF",
      justifyContent: "center",
      alignItems: "center",
      marginBottom: 20,
    },
    AppleButton: {
      height: 40,
      backgroundColor: "#000000",
      justifyContent: "center",
      alignItems: "center",
    },
    OrText: {
      fontSize: 20,
      fontWeight: "bold",
      textAlign: "center",
      marginVertical: 40,
    },
    input: {
      marginHorizontal: 30,
      borderWidth: 1,
      borderColor: "grey",
      padding: 10,
      marginBottom: 20,
    },
    password: {
      marginHorizontal: 30,
      borderWidth: 1,
      borderColor: "grey",
      padding: 10,
      flexDirection: "row",
      marginBottom: 10,
    },
    forgotText: {
      textAlign: "center",
    },
    bottom: {
      flex: 1,
      justifyContent: "flex-end",
      marginBottom: 40,
    },
    SignInButton: {
      backgroundColor: "#C4C4C4",
      height: 40,
      justifyContent: "center",
      alignItems: "center",
      marginHorizontal: 30,
      borderRadius: 25,
    },
    termsText: {
      marginBottom: 30,
      textAlign: "center",
    },
    checkboxContainer: {
      flexDirection: "row",
      margin: 20,
      alignItems: "center",
    },
    checkbox: {
      borderWidth: 1,
      borderColor: "grey",
      marginRight: 10,
      borderRadius: 10,
    },
    checkTextContainer: {
      width: "90%",
    },
  });
  