import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    FormContainer: {
        marginTop: 30,
      },
      RegisterViewContainer: {
        alignItems: "center",
      },
      FaceBookButton: {
        height: 40,
        backgroundColor: "#1877F2",
        justifyContent: "center",
        alignItems: "center",
        marginBottom: 20,
        minWidth: '82%',
        borderRadius: 5,
        flexDirection: "row"
      },
      AppleButton: {
        height: 40,
        backgroundColor: "#000000",
        justifyContent: "center",
        alignItems: "center",
        minWidth: '82%',
        borderRadius: 5,
        flexDirection: "row"
      },
      OrText: {
        fontSize: 16,
        fontWeight: "200",
        textAlign: "center",
        marginVertical: 30,
      },
      input: {
        marginHorizontal: 30,
        borderWidth: 1,
        borderColor: "rgba(189, 195, 199,1.0)",
        padding: 10,
        marginBottom: 10,
        borderRadius: 5,
        backgroundColor: "#fff"
      },
      password: {
        marginHorizontal: 30,
        borderWidth: 1,
        borderColor: "rgba(189, 195, 199,1.0)",
        padding: 10,
        flexDirection: "row",
        marginBottom: 10,
        borderRadius: 5,
        backgroundColor: "#fff"
      },
      forgotText: {
        textAlign: "center",
      },
      bottom: {
        flex: 1,
        justifyContent: "flex-end",
        marginBottom: 40,
      },
      SignInButton: {
        backgroundColor: "#e2c798",
        height: 40,
        justifyContent: "center",
        alignItems: "center",
        marginHorizontal: 30,
        borderRadius: 5,
      },
      termsText: {
        marginBottom: 30,
        textAlign: "center",
      },
      checkboxContainer: {
        flexDirection: "row",
        margin: 20,
        alignItems: "center",
      },
      checkbox: {
        borderWidth: 1,
        borderColor: "rgba(189, 195, 199,1.0)",
        marginRight: 10,
        borderRadius: 50,
    
      },
      checkTextContainer: {
        width: "90%",
      },
    });
    

   
    
