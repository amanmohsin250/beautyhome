import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    SignContainer: {
      marginTop: "5%"
      flex: 1,
    },
    rightContainer: {
        width: "20%"
    },

    TitleContainer: {
      flexDirection: "row",
      alignItems: "center",
      backgroundColor: "#fff",
      height: 50,
      ...Platform.select({
        ios: {
          shadowColor: '#000',
          shadowOffset: {width: 1, height: 3},
          shadowOpacity: 0.2,
        },
        android: {
          elevation: 4,
        },
      })
    },
    TitleText: {
      width: "60%",
      textAlign: "center",
      fontSize: 16,
      fontWeight: "500"
    },
    BackText: {
        fontSize: 12,
      fontWeight: "600"
    },
    TouchContainer: {
        width: "20%",
        display: "flex",
        flexDirection: "row",
        alignItems: "center"
    }
    
    
  });
