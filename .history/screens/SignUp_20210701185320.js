import React, { useState } from "react";
import {
  View,
  Modal,
  Text,
  TextInput,
  StyleSheet,
  Image,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Keyboard,
} from "react-native";
import PhoneInput from "react-native-phone-input";
import { MaterialCommunityIcons as Icons } from "@expo/vector-icons";
import { Checkbox } from "react-native-paper";
import AppbarStyles from './styles/AppbarStyles';
import Icon from 'react-native-vector-icons/FontAwesome';
import FormStyles from './styles/FormContainer';

export default function SignUp(props) {
  const [hidePass, setHidePass] = useState(true);
  const [Confirm, SetConfirm] = useState(false);
  const [checked, setChecked] = React.useState(false);
  const HandlePasswordVisibility = () => {
    setHidePass(!hidePass);
  };
  return (
    <Modal visible={props.visible} animationType="slide">
      {!Confirm ? (
        <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
          <View style={styles.SignUpContainer}>
            
            {/* Registration Bar */}
            <View style={AppbarStyles.TitleContainer}>
              <TouchableOpacity onPress={props.onBack} style={AppbarStyles.TouchContainer}>
                <Image source={require("../assets/images/Back.png")}/>
                <Text style={AppbarStyles.BackText}>Back</Text>
              </TouchableOpacity>
              <Text style={AppbarStyles.TitleText}>Register</Text>
              <View style={AppbarStyles.rightContainer}/>
            </View>

            <View style={FormStyles.FormContainer}>

              {/* Register Component */}
              <View style={FormStyles.RegisterViewContainer}>
                <TouchableOpacity activeOpacity={0.8}>
                  <View style={FormStyles.FaceBookButton}>
                    <Icon name="facebook-square" size={15} color="#fff" />
                    <Text style={{ marginLeft: 10, color: "white" }}>Register With Facebook</Text>
                  </View>
                </TouchableOpacity>

                <TouchableOpacity activeOpacity={0.8}>
                  <View style={FormStyles.AppleButton}>
                    <Icon name="apple" size={15} color="#fff" />
                    <Text style={{ marginLeft: 10, color: "white" }}>Register With Apple</Text>
                  </View>
                </TouchableOpacity>
              </View>
              {/* Register Component Ends */}
              
              <Text style={FormStyles.OrText}>OR</Text>

              <View style={FormStyles.input}>
                <TextInput placeholder="Email" />
              </View>
              <View style={FormStyles.password}>
                <TextInput
                  placeholder="Password"
                  secureTextEntry={hidePass}
                  style={{ flex: 1 }}
                />
                <TouchableOpacity onPress={HandlePasswordVisibility}>
                  <Icon name={hidePass ? "eye" : "eye-off"} size={20} />
                </TouchableOpacity>
              </View>
              <TouchableOpacity>
                <Text style={FormStyles.forgotText}>Forgot My Password?</Text>
              </TouchableOpacity>
            </View>
            <View style={FormStyles.bottom}>
              <TouchableOpacity>
                <Text style={FormStyles.termsText}>Terms and Conditions</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => SetConfirm(true)}>
                <View style={FormStyles.SignInButton}>
                  <Text style={{ fontSize: 16 }}>CONTINUE</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </TouchableWithoutFeedback>
      ) : (
        <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
          <View style={styles.SignUpContainer}>

            <View style={AppbarStyles.TitleContainer}>
              <TouchableOpacity onPress={() => SetConfirm(false)} style={AppbarStyles.TouchContainer}>
                <Image source={require("../assets/images/Back.png")} />
                <Text style={AppbarStyles.BackText}>Back</Text>
              </TouchableOpacity>
              <Text style={AppbarStyles.TitleText}>Confirm</Text>
              <View style={AppbarStyles.rightContainer}/>
            </View>

            <View style={FormStyles.FormContainer}>
              <View style={FormStyles.input}>
                <TextInput placeholder="First Name" />
              </View>
              <View style={FormStyles.input}>
                <TextInput placeholder="Last Name" />
              </View>
              <View style={FormStyles.input}>
                <PhoneInput />
              </View>
            </View>
            <View style={FormStyles.bottom}>
              <View style={FormStyles.checkboxContainer}>
                <View style={FormStyles.checkbox}>
                  <Checkbox
                    style={FormStyles.checkbox}
                    status={checked ? "checked" : "unchecked"}
                    onPress={() => {
                      setChecked(!checked);
                    }}
                  />
                </View>
                <View style={FormStyles.checkTextContainer}>
                  <Text style={{ fontSize: 12 }}>
                    By providing my details, I agree to receive treats, offers
                    and relevant content from this app
                  </Text>
                </View>
              </View>
              <TouchableOpacity onPress={() => SetConfirm(true)}>
                <View style={FormStyles.SignInButton}>
                  <Text style={{ fontSize: 16 }}>CONFIRM</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </TouchableWithoutFeedback>
      )}
    </Modal>
  );
}

const styles = StyleSheet.create({
  SignUpContainer: {
    marginTop: "10%",
    flex: 1,
    backgroundColor: "#f0eff5"
  }
});
