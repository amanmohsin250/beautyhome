import React, { useState } from "react";
import {
  View,
  Modal,
  Text,
  TextInput,
  StyleSheet,
  Image,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Keyboard,
} from "react-native";
import PhoneInput from "react-native-phone-input";
import { MaterialCommunityIcons as Icons } from "@expo/vector-icons";
import { Checkbox } from "react-native-paper";
import AppbarStyles from './styles/AppbarStyles';
import Icon from 'react-native-vector-icons/FontAwesome';

export default function SignUp(props) {
  const [hidePass, setHidePass] = useState(true);
  const [Confirm, SetConfirm] = useState(false);
  const [checked, setChecked] = React.useState(false);
  const HandlePasswordVisibility = () => {
    setHidePass(!hidePass);
  };
  return (
    <Modal visible={props.visible} animationType="slide">
      {!Confirm ? (
        <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
          <View style={styles.SignUpContainer}>
            
            {/* Registration Bar */}
            <View style={AppbarStyles.TitleContainer}>
              <TouchableOpacity onPress={props.onBack} style={AppbarStyles.TouchContainer}>
                <Image source={require("../assets/images/Back.png")}/>
                <Text style={AppbarStyles.BackText}>Back</Text>
              </TouchableOpacity>
              <Text style={AppbarStyles.TitleText}>Register</Text>
              <View style={AppbarStyles.rightContainer}/>
            </View>

            <View style={styles.FormContainer}>

              {/* Register Component */}
              <View style={styles.RegisterViewContainer}>
                <TouchableOpacity activeOpacity={0.8}>
                  <View style={styles.FaceBookButton}>
                    <Icon name="facebook-square" size={15} color="#fff" />
                    <Text style={{ marginLeft: 10, color: "white" }}>Register With Facebook</Text>
                  </View>
                </TouchableOpacity>

                <TouchableOpacity activeOpacity={0.8}>
                  <View style={styles.AppleButton}>
                    <Icon name="apple" size={15} color="#fff" />
                    <Text style={{ marginLeft: 10, color: "white" }}>Register With Apple</Text>
                  </View>
                </TouchableOpacity>
              </View>
              {/* Register Component Ends */}
              
              <Text style={styles.OrText}>OR</Text>
              <View style={styles.input}>
                <TextInput placeholder="Email" />
              </View>
              <View style={styles.password}>
                <TextInput
                  placeholder="password"
                  secureTextEntry={hidePass}
                  style={{ flex: 1 }}
                />
                <TouchableOpacity onPress={HandlePasswordVisibility}>
                  <Icon name={hidePass ? "eye" : "eye-off"} size={20} />
                </TouchableOpacity>
              </View>
              <TouchableOpacity>
                <Text style={styles.forgotText}>Forgot My Password?</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.bottom}>
              <TouchableOpacity>
                <Text style={styles.termsText}>Terms and Conditions</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => SetConfirm(true)}>
                <View style={styles.SignInButton}>
                  <Text style={{ fontSize: 20 }}>Continue</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </TouchableWithoutFeedback>
      ) : (
        <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
          <View style={styles.SignUpContainer}>
            <View style={styles.TitleContainer}>
              <TouchableOpacity onPress={() => SetConfirm(false)}>
                <Image source={require("../assets/images/Back.png")} />
              </TouchableOpacity>
              <Text style={styles.TitleText}>Confirm</Text>
            </View>
            <View style={styles.FormContainer}>
              <View style={styles.input}>
                <TextInput placeholder="First Name" />
              </View>
              <View style={styles.input}>
                <TextInput placeholder="Last Name" />
              </View>
              <View style={styles.input}>
                <PhoneInput />
              </View>
            </View>
            <View style={styles.bottom}>
              <View style={styles.checkboxContainer}>
                <View style={styles.checkbox}>
                  <Checkbox
                    style={styles.checkbox}
                    status={checked ? "checked" : "unchecked"}
                    onPress={() => {
                      setChecked(!checked);
                    }}
                  />
                </View>
                <View style={styles.checkTextContainer}>
                  <Text style={{ fontSize: 12 }}>
                    By providing my details, I agree to receive treats, offers
                    and relevant content from this app
                  </Text>
                </View>
              </View>
              <TouchableOpacity onPress={() => SetConfirm(true)}>
                <View style={styles.SignInButton}>
                  <Text style={{ fontSize: 20 }}>confirm</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </TouchableWithoutFeedback>
      )}
    </Modal>
  );
}

const styles = StyleSheet.create({
  SignUpContainer: {
    marginTop: "10%",
    flex: 1,
    backgroundColor: "#f0eff5"
  },
  RegisterViewContainer: {
    alignItems: "center",
  },
  FormContainer: {
    marginTop: 30,
  },
  FaceBookButton: {
    height: 40,
    backgroundColor: "#1877F2",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 20,
    minWidth: '80%',
    borderRadius: 5,
    flexDirection: "row"
  },
  AppleButton: {
    height: 40,
    backgroundColor: "#000000",
    justifyContent: "center",
    alignItems: "center",
    minWidth: '80%',
    borderRadius: 5,
    flexDirection: "row"
  },
  OrText: {
    fontSize: 16,
    fontWeight: "200",
    textAlign: "center",
    marginVertical: 30,
  },
  input: {
    marginHorizontal: 30,
    borderWidth: 1,
    borderColor: "rgba(189, 195, 199,1.0)",
    padding: 10,
    marginBottom: 10,
    borderRadius: 5,
    backgroundColor: "#fff"
  },
  password: {
    marginHorizontal: 30,
    borderWidth: 1,
    borderColor: "rgba(189, 195, 199,1.0)",
    padding: 8,
    flexDirection: "row",
    marginBottom: 10,
    borderRadius: 5,
    backgroundColor: "#fff"
  },
  forgotText: {
    textAlign: "center",
  },
  bottom: {
    flex: 1,
    justifyContent: "flex-end",
    marginBottom: 40,
  },
  SignInButton: {
    backgroundColor: "#C4C4C4",
    height: 40,
    justifyContent: "center",
    alignItems: "center",
    marginHorizontal: 30,
    borderRadius: 25,
  },
  termsText: {
    marginBottom: 30,
    textAlign: "center",
  },
  checkboxContainer: {
    flexDirection: "row",
    margin: 20,
    alignItems: "center",
  },
  checkbox: {
    borderWidth: 1,
    borderColor: "grey",
    marginRight: 10,
    borderRadius: 10,
  },
  checkTextContainer: {
    width: "90%",
  },
});
