import React, { useState } from "react";
import {
  View,
  Modal,
  Text,
  TextInput,
  StyleSheet,
  Image,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Keyboard,
} from "react-native";
import PhoneInput from "react-native-phone-input";
import { MaterialCommunityIcons as Icon } from "@expo/vector-icons";
import { Checkbox } from "react-native-paper";

export default function SignUp(props) {
  const [hidePass, setHidePass] = useState(true);
  const [Confirm, SetConfirm] = useState(false);
  const [checked, setChecked] = React.useState(false);
  const HandlePasswordVisibility = () => {
    setHidePass(!hidePass);
  };
  return (
    <Modal visible={props.visible} animationType="slide">
      {!Confirm ? (
        <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
          <View style={styles.SignUpContainer}>
            <View style={styles.TitleContainer}>
              <TouchableOpacity onPress={props.onBack}>
                <Image source={require("../assets/images/Back.png")} />
              </TouchableOpacity>
              <Text style={styles.TitleText}>Register</Text>
            </View>
            <View style={styles.FormContainer}>
              <TouchableOpacity activeOpacity={0.8}>
                <View style={styles.FaceBookButton}>
                  <Text style={{ color: "white" }}>Register With Facebook</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity activeOpacity={0.8}>
                <View style={styles.AppleButton}>
                  <Text style={{ color: "white" }}>Sign up With Apple</Text>
                </View>
              </TouchableOpacity>
              <Text style={styles.OrText}>OR</Text>
              <View style={styles.input}>
                <TextInput placeholder="Email" />
              </View>
              <View style={styles.password}>
                <TextInput
                  placeholder="password"
                  secureTextEntry={hidePass}
                  style={{ flex: 1 }}
                />
                <TouchableOpacity onPress={HandlePasswordVisibility}>
                  <Icon name={hidePass ? "eye" : "eye-off"} size={20} />
                </TouchableOpacity>
              </View>
              <TouchableOpacity>
                <Text style={styles.forgotText}>Forgot My Password?</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.bottom}>
              <TouchableOpacity>
                <Text style={styles.termsText}>Terms and Conditions</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => SetConfirm(true)}>
                <View style={styles.SignInButton}>
                  <Text style={{ fontSize: 20 }}>Continue</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </TouchableWithoutFeedback>
      ) : (
        <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
          <View style={styles.SignUpContainer}>
            <View style={styles.TitleContainer}>
              <TouchableOpacity onPress={() => SetConfirm(false)}>
                <Image source={require("../assets/images/Back.png")} />
              </TouchableOpacity>
              <Text style={styles.TitleText}>Confirm</Text>
            </View>
            <View style={styles.FormContainer}>
              <View style={styles.input}>
                <TextInput placeholder="First Name" />
              </View>
              <View style={styles.input}>
                <TextInput placeholder="Last Name" />
              </View>
              <View style={styles.input}>
                <PhoneInput />
              </View>
            </View>
            <View style={styles.bottom}>
              <View style={styles.checkboxContainer}>
                <View style={styles.checkbox}>
                  <Checkbox
                    style={styles.checkbox}
                    status={checked ? "checked" : "unchecked"}
                    onPress={() => {
                      setChecked(!checked);
                    }}
                  />
                </View>
                <View style={styles.checkTextContainer}>
                  <Text style={{ fontSize: 12 }}>
                    By providing my details, I agree to receive treats, offers
                    and relevant content from this app
                  </Text>
                </View>
              </View>
              <TouchableOpacity onPress={() => SetConfirm(true)}>
                <View style={styles.SignInButton}>
                  <Text style={{ fontSize: 20 }}>confirm</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </TouchableWithoutFeedback>
      )}
    </Modal>
  );
}

const styles = StyleSheet.create({
  SignUpContainer: {
    marginTop: "10%",
    flex: 1,
  },
  TitleContainer: {
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "#fff",
    height: 50,
    ...Platform.select({
      ios: {
        shadowColor: '#000',
        shadowOffset: {width: 1, height: 3},
        shadowOpacity: 0.2,
      },
      android: {
        elevation: 4,
      },
    })
  },
  TitleText: {
    width: "90%",
    textAlign: "center",
    fontSize: 18,
    fontWeight: 'bold'
  },
  FormContainer: {
    marginTop: 30,
  },
  FaceBookButton: {
    height: 40,
    backgroundColor: "#2A89BF",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 20,
  },
  AppleButton: {
    height: 40,
    backgroundColor: "#000000",
    justifyContent: "center",
    alignItems: "center",
  },
  OrText: {
    fontSize: 20,
    fontWeight: "bold",
    textAlign: "center",
    marginVertical: 40,
  },
  input: {
    marginHorizontal: 30,
    borderWidth: 1,
    borderColor: "grey",
    padding: 10,
    marginBottom: 20,
  },
  password: {
    marginHorizontal: 30,
    borderWidth: 1,
    borderColor: "grey",
    padding: 10,
    flexDirection: "row",
    marginBottom: 10,
  },
  forgotText: {
    textAlign: "center",
  },
  bottom: {
    flex: 1,
    justifyContent: "flex-end",
    marginBottom: 40,
  },
  SignInButton: {
    backgroundColor: "#C4C4C4",
    height: 40,
    justifyContent: "center",
    alignItems: "center",
    marginHorizontal: 30,
    borderRadius: 25,
  },
  termsText: {
    marginBottom: 30,
    textAlign: "center",
  },
  checkboxContainer: {
    flexDirection: "row",
    margin: 20,
    alignItems: "center",
  },
  checkbox: {
    borderWidth: 1,
    borderColor: "grey",
    marginRight: 10,
    borderRadius: 10,
  },
  checkTextContainer: {
    width: "90%",
  },
});
