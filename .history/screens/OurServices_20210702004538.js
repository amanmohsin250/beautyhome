import React, { useState } from "react";
import {
  View,
  Modal,
  Text,
  TextInput,
  StyleSheet,
  Image,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import { Chip, Card, Title } from "react-native-paper";
import AppbarStyles from "./styles/AppbarStyles";
import { Tab } from 'react-native-elements';

export default function OurServices(props) {
  const [ActiveTab, setActiveTab] = useState("men");
  const man = [
    {
      cardTitle: "Men essential Massage",
      cardImage: require("../assets/images/splash1.jpg"),
    },
    {
      cardTitle: "Men Hair Color",
      cardImage: require("../assets/images/splash2.jpg"),
    },
    {
      cardTitle: "Men Cut and Blow Dry",
      cardImage: require("../assets/images/splash3.jpg"),
    },
    {
      cardTitle: "Mens Waxing",
      cardImage: require("../assets/images/splash5.jpg"),
    },
    {
      cardTitle: "Mens Facials",
      cardImage: require("../assets/images/homepage2.jpg"),
    },
  ];
  const womens = [
    {
      cardTitle: "Women essential Massage",
      cardImage: require("../assets/images/splash1.jpg"),
    },
    {
      cardTitle: "Women Hair Color",
      cardImage: require("../assets/images/splash2.jpg"),
    },
    {
      cardTitle: "Women Cut and Blow Dry",
      cardImage: require("../assets/images/splash3.jpg"),
    },
    {
      cardTitle: "Women Waxing",
      cardImage: require("../assets/images/splash5.jpg"),
    },
    {
      cardTitle: "Women Facials",
      cardImage: require("../assets/images/homepage1.jpg"),
    },
    {
      cardTitle: "Lashes",
      cardImage: require("../assets/images/homepage2.jpg"),
    },
    {
      cardTitle: "Nails",
      cardImage: require("../assets/images/homepage3.jpg"),
    },
    {
      cardTitle: "Brows",
      cardImage: require("../assets/images/homepage1.jpg"),
    },
  ];
  const packages = [
    // "essential Massage Package",
    // "Hair Color Package",
    // "Cut and Blow Dry Package",
    // "Waxing Package",
    // "Facials Package",
    // "Lashes Package",
    // "Nails Package",
    // "Brows Package",
    {
      cardTitle: "essential Massage Package",
      cardImage: require("../assets/images/splash1.jpg"),
    },
    {
      cardTitle: "Hair Color Package",
      cardImage: require("../assets/images/splash2.jpg"),
    },
    {
      cardTitle: "Cut and Blow Dry Package",
      cardImage: require("../assets/images/splash3.jpg"),
    },
    {
      cardTitle: "Waxing Package",
      cardImage: require("../assets/images/splash5.jpg"),
    },
    {
      cardTitle: "Facials Package",
      cardImage: require("../assets/images/homepage1.jpg"),
    },
    {
      cardTitle: "Lashes Package",
      cardImage: require("../assets/images/homepage2.jpg"),
    },
    {
      cardTitle: "Nails Package",
      cardImage: require("../assets/images/homepage3.jpg"),
    },
    {
      cardTitle: "Brows Package",
      cardImage: require("../assets/images/homepage1.jpg"),
    },
  ];
  return (
    <Modal visible={props.visible} animationType="slide">
      <View style={styles.ServicesContainer}>
        {/* Registration Bar */}
        <View style={AppbarStyles.TitleContainer}>
            <TouchableOpacity onPress={props.onBack} style={AppbarStyles.TouchContainer}>
              <Image source={require("../assets/images/Back.png")}/>
              <Text style={AppbarStyles.BackText}>Back</Text>
            </TouchableOpacity>
            <Text style={AppbarStyles.TitleText}>Our Services</Text>
            <View style={AppbarStyles.rightContainer}/>
        </View>

        

        <View style={styles.TabsContainer}>

          <TouchableOpacity onPress={() => setActiveTab("men")}>
            <Chip style={ActiveTab === "men" ? styles.active : styles.inactive}>
              <Text style={ActiveTab === "men" ? styles.TabText : null}>
                Men
              </Text>
            </Chip>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => setActiveTab("women")}>
            <Chip
              style={ActiveTab === "women" ? styles.active : styles.inactive}
            >
              <Text style={ActiveTab === "women" ? styles.TabText : null}>
                Women
              </Text>
            </Chip>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => setActiveTab("package")}>
            <Chip
              style={ActiveTab === "package" ? styles.active : styles.inactive}
            >
              <Text style={ActiveTab === "package" ? styles.TabText : null}>
                Packages
              </Text>
            </Chip>
          </TouchableOpacity>
        </View>

        <ScrollView>
          {ActiveTab === "men"
            ? man.map((item, index) => (
                <View key={index} style={styles.listItems}>
                  <Card style={{ marginHorizontal: 15 }}>
                    <Card.Cover source={item.cardImage} />
                    <Card.Content>
                      <Title>{item.cardTitle}</Title>
                    </Card.Content>
                  </Card>
                </View>
              ))
            : ActiveTab === "women"
            ? womens.map((item, index) => (
                <View key={index} style={styles.listItems}>
                  <Card style={{ marginHorizontal: 15 }}>
                    <Card.Cover source={item.cardImage} />
                    <Card.Content>
                      <Title>{item.cardTitle}</Title>
                    </Card.Content>
                  </Card>
                </View>
              ))
            : packages.map((item, index) => (
                <View key={index} style={styles.listItems}>
                  <Card style={{ marginHorizontal: 15 }}>
                    <Card.Cover source={item.cardImage} />
                    <Card.Content>
                      <Title>{item.cardTitle}</Title>
                    </Card.Content>
                  </Card>
                </View>
              ))}
        </ScrollView>
      </View>
    </Modal>
  );
}

const styles = StyleSheet.create({
  ServicesContainer: {
    marginTop: "10%",
    flex: 1,
    backgroundColor: "#f0eff5"
  },
  TitleContainer: {
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "#C4C4C4",
    height: 50,
  },
  TitleText: {
    width: "90%",
    textAlign: "center",
  },
  TabsContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginVertical: 20,
    marginHorizontal: 10,
  },
  active: {
    backgroundColor: "#a379b6",
    borderRadius: 1,
  },
  TabText: {
    color: "white",
  },
  listItems: {
    marginVertical: 10,
    // marginHorizontal: 20,
    // borderColor: "grey",
    // borderWidth: 1,
    // padding: 15,
    // backgroundColor: "#C4C4C4",
  },
});
