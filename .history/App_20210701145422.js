import React, { useState } from "react";
import { StyleSheet, Text, View, TouchableOpacity, Button } from "react-native";
import MainScreen from "./screens/MainScreen";
import Service from "./screens/Service";
import ServiceDetails from "./screens/ServiceDetails";
import DateTimeScreen from "./screens/DateTime";
import PaymentScreen from "./screens/Payment";
import ConfirmationScreen from "./screens/Confirmation";
import ProfileScreen from "./screens/Profile";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import HomeDrawer from "./screens/HomeDrawer";
import { Ionicons } from "@expo/vector-icons";

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer >
      <Stack.Navigator initialRouteName="Main" headerMode="none">
        <Stack.Screen name="Main" component={MainScreen} />
        <Stack.Screen name="Home" component={HomeDrawer} />
        <Stack.Screen name="Service" component={Service} />
        <Stack.Screen name="Profile" component={ProfileScreen} />
        <Stack.Screen
          name="DateTime"
          component={DateTimeScreen}
          options={{
            title: "Select date and time",
          }}
        />
        <Stack.Screen
          name="Confirmation"
          component={ConfirmationScreen}
          options={{
            title: "Confirmation",
          }}
        />
        <Stack.Screen
          name="Payment"
          component={PaymentScreen}
          options={{
            title: "Checkout",
          }}
        />
        <Stack.Screen
          name="ServiceDetails"
          component={ServiceDetails}
          options={{
            title: "Service detail + Add on",
            headerStyle: {
              backgroundColor: "#F4CFB4",
            },
            headerTintColor: "#fff",
            headerTitleStyle: {
              fontWeight: "bold",
            },
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
  //<MainScreen />;
}

const styles = StyleSheet.create({});
