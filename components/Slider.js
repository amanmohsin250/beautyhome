import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  Dimensions,
} from "react-native";

const { width } = Dimensions.get("window");
const height = (width * 100) / 90;

export default function Slider(props) {
  const [active, setActive] = useState(0);
  const ScrollHandler = ({ nativeEvent }) => {
    const slide = Math.ceil(
      nativeEvent.contentOffset.x / nativeEvent.layoutMeasurement.width
    );
    if (slide !== active) {
      setActive(slide);
    }
  };
  return (
    <View style={styles.container}>
      <ScrollView
        horizontal
        pagingEnabled
        onScroll={ScrollHandler}
        showsHorizontalScrollIndicator={false}
        style={styles.container}
      >
        {props.images.map((image, index) => (
          <Image key={index} source={image} style={styles.image} />
        ))}
      </ScrollView>
      <View style={styles.pagination}>
        {props.images.map((i, k) => (
          <Text
            key={k}
            style={k === active ? styles.pagingActiveText : styles.pagingText}
          >
            ⬤
          </Text>
        ))}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: { width, height },
  image: { width, height, resizeMode: "contain" },
  pagination: { flexDirection: "row", justifyContent: "center" },
  pagingText: { color: "lightgrey", fontSize: width / 30, margin: 3 },
  pagingActiveText: { color: "grey", fontSize: width / 30, margin: 3 },
});
